package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        int smallest;
        int num = 0;
        int change = 0 ;
        int n =  0 ;
        boolean bool = false;
        
        for( int i = n ; i < size ; i++) {
            for ( int j = n ; j < size ; j++) {
            if(arr[i] < arr[j]) {
                change ++;
            }
            if ( change == size-1) {
                smallest = arr[i];
            arr[num] = smallest;
            num++;
            bool = true;
            
            }
            if (bool) {
                n++;
            }
        
        }
        }
        return arr;
    
        
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        int change = 0 ;
        boolean bool = false;
        for ( int i = 0 ; i < size ; i++) {
            if (arr[i] > arr[i+1]) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                bool = true;

            }
            if(bool) {
                for ( int j = i ; j >= 0 ; j--) {
                    if ( arr[i] < arr[j]) {
                        change++;

                    }
                }
                for ( int k = i-change ; k < size ; k++) {
                     arr[k+1] = arr[k] ; 
                }
                arr[i-change] = arr[i];

            }
            bool = false;

        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        int length1 = size /2;
        int length2 = size / 2 ;
        int[] ar1 = new int[length1];
        int[] ar2 = new int[length2];
        for ( int i = 0 ; i < length1; i++) {
            ar1[i] = arr[i] ; 
        }
        for ( int j = length1 ; j < size ; j++) {
            ar2[j] = arr[j];
        }
        //sort ar1
        for ( int i = 0 ; i < length1 ; i++) {
            for ( int j = i + 1 ; j < length1 ; j++) {
                if (ar1[i] > ar1[j]) {
                    int temp1 ; 
                    temp1 = ar1[i];
                    ar1[i] = ar1[j];
                    ar1[j] = temp1 ; 
                }
            }
        }
        //sort ar2
        for ( int  i = 0 ; i < length2 ; i++) {
             for ( int j = i + 1 ; j < length2 ; j++) {
                 if (ar2[i] > ar2[j]) {
                     int temp2 ; 
                     temp2 = ar2[i];
                     ar2[i] = ar2[j];
                     ar2[j] = temp2;
                 }
             }
        }
       
        // sort all

        int num = 0 ;
        int num1 = 0 ; 
        int counter1 = 0 ; 
        int counter2 = 0 ;
        for ( int i = 0 ; i < length1 ; i++) {
            counter1 ++;
            for ( int j = num1 ; j < length2 ; j++) {
                counter2++;
                int count  = 0 ;
                if (ar1[i] < ar2[j]) {
                    arr[num] = ar1[i];
                    break;
                }
                if(ar1[i] > ar2[j]) {
                    arr[num] = ar2[i];
                    num1++;

                     
                }
                num++;
                
                
            }
        
        }
        int counters = counter1 + counter2;
        int missednumbers = size - counters ;
         //find which array has the bigger maximum 
        int max1 = Maxofarray(ar1 , length1);
        int max2 = Maxofarray(ar2 , length2);
        if ( max1 > max2) {
            for ( int i = missednumbers ; i < size;i++) {
                int j = ((size - missednumbers)/2) - 1;
                arr[i] = ar1[j];
                j++;
            }

        }
        else {
            for ( int i = missednumbers ; i < size ; i++) {
                int j = ((size - missednumbers)/2) - 1;
                arr[i] = ar2[j] ;
                j++;
            }
        }
        

    return arr;
    }
    public int Maxofarray (int[] ar1 , int length1 ) {
        int maximum = ar1[0] ; 
        for ( int i = 0 ; i < length1 ; i++ ) {
            if (ar1[i] > maximum)
            maximum = ar1[i];
        }
        return maximum;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int length = arr.length;
        int target = (length-1)/2;
        while (target >= 0 ) {
            if (value > arr[target]) {
                target = (target-1) / 2 ;
            }
            else {
                target = (target+1) / 2 ;
            }

        }
        if ( value == arr[target]) {
            return arr[target];
        }
        else {
            return -1;
        }
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
         int length = arr.length;
        int target = (length-1)/2;
        int number = 0 ; 
        number += target;
        while (target >= 0 ) {
            if (value > arr[target]) {
                target = (target-1) / 2 ;
            }
            else {
                target = (target+1) / 2 ;
            }
            number += target;

        }
        if ( value == arr[target]) {
            return number;
        }
        else {
            return -1;
        }
        
    }
}
