public class whitefunctions {
    public String  whiteone (String text1 , String text2) {
        int length1 = text1.length();
        int length2 = text2.length();
        int length = length1 + length2;
        char[] arrcombine = new char[length];
        if ( length1 == length2) {
            int num1 = 0 ;
            int num2 = 0 ;
            for ( int i = 0 ; i < length ; i++) {
                if ( i % 2 == 0 ) {
                    arrcombine[i] = text1.charAt(num1);
                    num1++;
                }
                if ( i % 2 != 0) {
                    arrcombine[i] = text2.charAt(num2);
                    num2++;
                }
            }

        }
        if (length1 > length2) {
            int num1  = 0 ;
            int num2 = 0 ;
            int n = length1 - length2;
            int newlength = length - n;
            for ( int i = 0 ; i < newlength ;i++ ) {
                if ( i % 2 == 0 ) {
                    arrcombine[i] = text1.charAt(num1);
                    num1++;
                }
                if ( i % 2 != 0) {
                    arrcombine[i] = text2.charAt(num2);
                    num2++;
                }
            }
            for ( int j = newlength ; j < length ; j++ ) {
                arrcombine[j] = text1.charAt(j);
            }

        }
        if (length1 < length2) {
            int num1  = 0 ;
            int num2 = 0 ;
            int n = length2 - length1;
            int newlength = length - n;
            for ( int i = 0 ; i < newlength ;i++ ) {
                if ( i % 2 == 0 ) {
                    arrcombine[i] = text1.charAt(num1);
                    num1++;
                }
                if ( i % 2 != 0) {
                    arrcombine[i] = text2.charAt(num2);
                    num2++;
                }
            }
            for ( int j = newlength ; j < length ; j++ ) {
                arrcombine[j] = text2.charAt(num2);
                num2++;
            }
        }
        String answer = new String(arrcombine);
         return answer;
    }

    public String  whitetwo (String text1 , String text2) {
        int length2 = text2.length();
        int length1 = text1.length();
        int length = length1 + length2;
        char[] arrreverse = new char[length2];
        for (int i = 0; i < length2; i++) {
            arrreverse[i] = text2.charAt(i);
        }
        char[] temp = new char[length2];
        for (int j = 0; j < length2; j++) {
            temp[length2 -1 - j] = arrreverse[j];
        }
        text2 = new String(temp);

        char[] newarr = new char[length];
        int num = 0;
        for ( int i = 0 ; i < length ; i++) {
            if ( i < length1) {
                newarr[i] = text1.charAt(i);
            }
            else {
                newarr[i] = text2.charAt(num);
                num++;
            }
        }
        String answer = new String(newarr);
        return answer;
    }

    public String whitethree (String text1 , String text2) {
        int length1 = text1.length();
        int length2 = text2.length();
        int length = length1 + length2;
        char[] arr = new char[length];
        if (length1 == length2) {
            int num1 = 0;
            int num2 = 0;
            char[] arrreverse = new char[length2];
            for (int i = 0; i < length2; i++) {
                arrreverse[i] = text2.charAt(i);
            }
            char[] temp = new char[length2];
            for (int j = 0; j < length2; j++) {
                temp[length2 - 1 - j] = arrreverse[j];
            }
            text2 = new String(temp);
            for (int i = 0; i < length; i++) {
                if (i % 2 == 0) {
                    arr[i] = text1.charAt(num1);
                    num1++;
                } else {
                    arr[i] = text2.charAt(num2);
                    num2++;
                }

            }

        }
        if (length1 > length2) {
            int num1  = 0;
            int num2 = 0;
            int n = length1 - length2;
            int newlength = length - n;

            char[] arrreverse = new char[length2];
            for (int i = 0; i < length2; i++) {
                arrreverse[i] = text2.charAt(i);
            }
            char[] temp = new char[length2];
            for (int j = 0; j < length2; j++) {
                temp[length2 - 1 - j] = arrreverse[j];
            }
            text2 = new String(temp);
            for (int i = 0; i < newlength; i++) {
                if (i % 2 == 0) {
                    arr[i] = text1.charAt(num1);
                    num1++;
                } else {
                    arr[i] = text2.charAt(num2);
                    num2++;
                }

            }
            for ( int j = newlength ; j < length ; j++) {
                arr[j] = text1.charAt(num1);
                num1++;
            }
        }
        if (length2 > length1) {
            int num1  = 0;
            int num2 = 0;
            int n = length2 - length1;
            int newlength = length - n;

            char[] arrreverse = new char[length2];
            for (int i = 0; i < length2; i++) {
                arrreverse[i] = text2.charAt(i);
            }
            char[] temp = new char[length2];
            for (int j = 0; j < length2; j++) {
                temp[length2 - 1 - j] = arrreverse[j];
            }
            text2 = new String(temp);
            for (int i = 0; i < newlength; i++) {
                if (i % 2 == 0) {
                    arr[i] = text1.charAt(num1);
                    num1++;
                } else {
                    arr[i] = text2.charAt(num2);
                    num2++;
                }

            }
            for ( int j = newlength ; j < length ; j++) {
                arr[j] = text2.charAt(num2);
                num2++;
            }
        }
        String answer = new String(arr);
        return answer;

    }

    public String whitefour (String text1 , String text2) {
        int length1 = text1.length();
        if ( length1 % 2 == 0 ) {
            return text1;
        }
        else {
            return text2;
        }
    }

    public String whitefive (String text1 , String text2) {
        int length1 = text1.length();
        int length2 = text2.length();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int lengthofalphabet = alphabet.length();
        int[] number1 = new int[length1];
        int[] number2 = new int[length2];
        int num1 = 0 ;
        int num2 = 0;
        String answer = " ";
        if ( length1 == length2) {
            for ( int i = 0 ; i < length1 ; i++ ) {
                for ( int j = 0 ; j < lengthofalphabet ; j++) {
                    if (text1.charAt(i) == alphabet.charAt(j)) {
                        number1[num1] = j;
                        num1++;
                    }
                }
            }
            for ( int t = 0 ; t < length2 ; t++) {
                for ( int k = 0 ; k < lengthofalphabet; k++) {
                    if (text2.charAt(t) == alphabet.charAt(k)) {
                        number2[num2] = k;
                        num2++;
                    }
                }
            }
            int[] plus = new int[length1];
            for ( int p = 0 ; p < length1 ; p++) {
                plus[p] = number1[p] + number2[p] ;
            }
            char[] newstring = new char[length1];
            for ( int i = 0 ; i < length1; i++) {
                newstring[i] = alphabet.charAt( plus[i] % 26 );
            }
            answer = new String(newstring);



        }

        if ( length1 > length2) {

            for ( int i = 0 ; i < length1 ; i++ ) {
                for ( int j = 0 ; j < lengthofalphabet ; j++) {
                    if (text1.charAt(i) == alphabet.charAt(j)) {
                        number1[num1] = j;
                        num1++;
                    }
                }
            }
            for ( int t = 0 ; t < length2 ; t++) {
                for ( int k = 0 ; k < lengthofalphabet; k++) {
                    if (text2.charAt(t) == alphabet.charAt(k)) {
                        number2[num2] = k;
                        num2++;
                    }
                }
            }

            int[] plus = new int[length2];
            for ( int i = 0 ; i < length2 ; i++) {
                plus[i] = number1[i] + number2[i];
            }

            char[] newstring = new char[length1];
            for ( int j = 0 ; j < length2 ; j++) {
                newstring[j] = alphabet.charAt(plus[j]);
            }
            for ( int t = length2 ; t < length1 ; t++) {
                newstring[t] = alphabet.charAt(number1[t]);
            }
            answer = new String(newstring);



        }
        if ( length2 > length1) {
            for ( int i = 0 ; i < length1 ; i++ ) {
                for ( int j = 0 ; j < lengthofalphabet ; j++) {
                    if (text1.charAt(i) == alphabet.charAt(j)) {
                        number1[num1] = j;
                        num1++;
                    }
                }
            }
            for ( int t = 0 ; t < length2 ; t++) {
                for ( int k = 0 ; k < lengthofalphabet; k++) {
                    if (text2.charAt(t) == alphabet.charAt(k)) {
                        number2[num2] = k;
                        num2++;
                    }
                }
            }

            int[] plus = new int[length1];
            for ( int i = 0 ; i <length1 ; i++) {
                plus[i] = number1[i] + number2[i];
            }
            char[] newstring = new char[length2];
            for ( int j = 0 ; j < length1 ; j++) {
                newstring[j] = alphabet.charAt(plus[j]);
            }
            for ( int t = length1 ; t < length2 ; t++) {
                newstring[t] = alphabet.charAt(number2[t]);
            }
            answer = new String(newstring);

        }
        return answer;
    }
}