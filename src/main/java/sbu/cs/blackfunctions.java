public class  blackfunctions {

    public String Blackone(String text) {
        int length = text.length();
        char[] arrreverse = new char[length];
        for (int i = 0; i < length; i++) {
            arrreverse[i] = text.charAt(i);
        }
        char[] temp = new char[length];
        for (int j = 0; j < length; j++) {
            temp[length -1 - j] = arrreverse[j];
        }
        String answer = new String(temp);
        return answer;
    }

    public String Blacktwo(String text) {
        int length = text.length();
        char[] arrrepeat = new char[length];
        for (int i = 0; i < length ; i++) {
            arrrepeat[i] = text.charAt(i);
        }
        int newlength = (length * 2);
        char[] temp = new char[newlength];
        int num = 0;
        for (int j = 0; j < newlength - 1 ; j = j+2) {
            temp[j] = temp[j + 1] = arrrepeat[num];
            num++;
        }
        String answer = new String(temp);
        return answer;
    }

    public String  Blackthree(String text) {
        int length = text.length();
        char[] arrrepeatwhole = new char[length];
        for (int i = 0; i < length; i++) {
            arrrepeatwhole[i] = text.charAt(i);
        }
        int newlength = length * 2;
        char[] temp = new char[newlength];
        int num = 0;
        for (int j = 0; j < length; j++) {
            temp[j] = temp[length + j] = arrrepeatwhole[num];
            num++;
        }
        String answer = new String(temp);
        return answer;

    }

    public String  Blackfour(String text) {
        int length = text.length();
        char[] arradd = new char[length];
        for (int i = 0; i < length; i++) {
            arradd[i] = text.charAt(i);
        }
        char[] temp = new char[length];
        for (int j = 0; j < length - 1; j++) {
            temp[j + 1] = arradd[j];
        }
        temp[0] = arradd[length-1];
        String answer = new String(temp);
        return answer;
    }

    public String  Blackfive (String text) {
        int length = text.length();
        char[] arrconvert = new char[length];
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int lengthofalphabet = alphabet.length();
        char[] temp = new char[length];
        int num = 0;
        for ( int i = 0 ; i < length ; i++) {
            for ( int j = 0 ; j < lengthofalphabet ; j++) {
                if (text.charAt(i) == alphabet.charAt(j)) {
                    temp[num] = alphabet.charAt(lengthofalphabet -1 - j);
                    num++;
                }
            }
        }
        String answer = new String(temp);
        return answer;

    }

}